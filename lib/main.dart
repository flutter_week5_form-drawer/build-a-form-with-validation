import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Form Validate';
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: const Text(appTitle),
      ),
      body: const MyCustomForm(),
    ));
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

class _MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  final double padding = 15.0;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      // autovalidateMode: AutovalidateMode.always,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(padding),
            child: TextFormField(
              decoration: InputDecoration(
                hintText: 'Enter Text',
                border: OutlineInputBorder(),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(padding),
            child: TextFormField(
              decoration: InputDecoration(
                labelText: 'Enter your Email',
                helperText: 'example user@mail.com',
                border: UnderlineInputBorder(),
              ),
              validator: (value)
                  // =>
                  //     EmailValidator.validate(value) ? null : 'Please enter email'
                  {
                if (EmailValidator.validate(value!)) {
                  return null;
                }
                return 'Please enter email';
              },
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: EdgeInsets.all(padding),
            child: SizedBox(
              width: double.infinity,
              height: 30.0,
              child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: const Text('Processing Data')));
                    }
                  },
                  child: const Text('Submit')),
            ),
          )
        ],
      ),
    );
  }
}
